package com.myorders.makeorder.data_entities;

public class StatusWrapper {
    public static enum Status {IDDLE, PROCCESSING, SUCCESS, ERROR}

    private Status status = Status.IDDLE;
    private String responseString;

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getResponseString() {
        return responseString;
    }

    public void setResponseString(String responseString) {
        this.responseString = responseString;
    }
}
