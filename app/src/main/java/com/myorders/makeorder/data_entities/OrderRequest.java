package com.myorders.makeorder.data_entities;


import android.content.Context;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class OrderRequest {
    public static final String KEY_UUID = "uuid";
    private static Gson gson = new Gson();

    public String toJson() {
        this.getData().setDateToDeliver(""+(System.currentTimeMillis()+86400*1000));
        return gson.toJson(this);
    }

    public OrderRequest(Context context) {
        initDefaults(context);
    }

    public void initDefaults(Context context) {
        header = new Header();
        this.getHeader().setVersion("2.0");

        method = new Method();
        this.getMethod().setMtime(0);
        this.getMethod().setMode("getData");
        this.getMethod().setName("makeOrder");

        data = new Data();
        this.getData().setDeliveryAddress(new DeliveryAddress());
        this.getData().getDeliveryAddress().setCityId("1");
        this.getData().setNotificationType("СМС оповещение");
        this.getData().setDeliveryType("deliveryTypeRegular");
        this.getData().setPaymentMethod("payment_card_restaurant");

        this.getData().setSender(new Sender());
        this.getData().setPersons("1");
        List<OrderItem> list = new ArrayList<>();
        OrderItem orderItem = new OrderItem();
        orderItem.setItemId("9");
        orderItem.setPrice(110);
        orderItem.setAmount("1");
        list.add(orderItem);
        this.data.setOrderItems(list);
        String uid = PreferenceManager.getDefaultSharedPreferences(context)
                .getString(KEY_UUID, null);
        getHeader().setUserId(uid);
        Agent agent = new Agent();
        agent.setDevice("desktop");
        agent.setVersion("Chromium 68.0.3440.75");
        getHeader().setAgent(agent);
    }

    @SerializedName("data")
    @Expose
    private Data data;
    @SerializedName("method")
    @Expose
    private Method method;
    @SerializedName("header")
    @Expose
    private Header header;


    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public Method getMethod() {
        return method;
    }

    public void setMethod(Method method) {
        this.method = method;
    }

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

}

