package com.myorders.makeorder.data_entities.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Response {

    @SerializedName("method")
    @Expose
    private String method;
    @SerializedName("Result")
    @Expose
    private Result result;
    @SerializedName("last_modified")
    @Expose
    private Integer lastModified;

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public Integer getLastModified() {
        return lastModified;
    }

    public void setLastModified(Integer lastModified) {
        this.lastModified = lastModified;
    }

}
