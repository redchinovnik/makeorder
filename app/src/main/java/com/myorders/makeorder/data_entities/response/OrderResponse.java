package com.myorders.makeorder.data_entities.response;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class OrderResponse {

    @SerializedName("Response")
    @Expose
    private Response response;
    @SerializedName("ResponseBody")
    @Expose
    private ResponseBody responseBody;

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

    public ResponseBody getResponseBody() {
        return responseBody;
    }

    public void setResponseBody(ResponseBody responseBody) {
        this.responseBody = responseBody;
    }

}