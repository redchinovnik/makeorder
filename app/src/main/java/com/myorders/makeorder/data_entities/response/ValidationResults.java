package com.myorders.makeorder.data_entities.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ValidationResults {

    @SerializedName("Result")
    @Expose
    private Integer result;
    @SerializedName("Errors")
    @Expose
    private List<Object> errors = null;

    public Integer getResult() {
        return result;
    }

    public void setResult(Integer result) {
        this.result = result;
    }

    public List<Object> getErrors() {
        return errors;
    }

    public void setErrors(List<Object> errors) {
        this.errors = errors;
    }

}
