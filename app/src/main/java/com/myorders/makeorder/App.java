package com.myorders.makeorder;

import android.app.Application;
import android.preference.PreferenceManager;

import com.myorders.makeorder.data_entities.OrderRequest;
import com.myorders.makeorder.di.AppComponent;
import com.myorders.makeorder.di.AppModule;
import com.myorders.makeorder.di.DaggerAppComponent;

import java.util.UUID;

public class App extends Application {
    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        initUID();
        initComponents();
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }

    private void initComponents() {
        appComponent = DaggerAppComponent.builder().appModule(new AppModule(this)).build();
    }

    private void initUID() {
        if (PreferenceManager.getDefaultSharedPreferences(this).getString(OrderRequest.KEY_UUID, null) == null) {
            UUID uuid = UUID.randomUUID();
            PreferenceManager.getDefaultSharedPreferences(this).edit()
                    .putString(OrderRequest.KEY_UUID, uuid.toString())
                    .commit();
        }
    }
}