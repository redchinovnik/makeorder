package com.myorders.makeorder.di;

import com.myorders.makeorder.user_interface.OrderFragment;
import com.myorders.makeorder.user_interface.SuccessFragment;

import javax.inject.Singleton;

import dagger.Component;

@Component(modules = {AppModule.class})
@Singleton
public interface AppComponent {
    void injectFragment(OrderFragment fragment);

    void injectSuccessFragment(SuccessFragment fragment);
}
