package com.myorders.makeorder.di;

import android.content.Context;

import com.myorders.makeorder.OrderMaker;
import com.myorders.makeorder.OrderRESTService;
import com.myorders.makeorder.OrderViewModel;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

@Module
public class AppModule {
    private Context context;

    public AppModule(Context context) {
        this.context = context;
    }

    @Provides
    @Singleton
    public OrderRESTService provideOrderService() {
        Retrofit retrofit = new Retrofit.Builder().baseUrl("http://api.dev2.tanuki.ru")
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();
        return retrofit.create(OrderRESTService.class);
    }

    @Provides
    @Singleton
    public OrderMaker provideOrderMaker(OrderRESTService orderRESTService) {
        return new OrderMaker(orderRESTService);
    }

    @Provides
    @Singleton
    public OrderViewModel provideModel(OrderMaker orderMaker) {
        return new OrderViewModel(orderMaker);
    }
}
