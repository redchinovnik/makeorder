package com.myorders.makeorder;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Build;

import com.myorders.makeorder.user_interface.SuccessActivity;

import dagger.Subcomponent;

public class NavigationController {

    public static void navigateToSuccess(Activity activity) {
        Intent intent = SuccessActivity.getCallingIntent(activity);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            activity.startActivity(intent,
                    ActivityOptions.makeSceneTransitionAnimation(activity).toBundle());
        } else {
            activity.startActivity(intent);
        }
    }

    public static void navigateToOrder(Activity activity) {
        Intent intent = OrderActivity.getCallingIntent(activity);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            activity.startActivity(intent,
                    ActivityOptions.makeSceneTransitionAnimation(activity).toBundle());
        } else {
            activity.startActivity(intent);
        }
    }
}
