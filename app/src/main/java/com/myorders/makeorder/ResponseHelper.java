package com.myorders.makeorder;

import com.google.gson.Gson;
import com.myorders.makeorder.data_entities.response.OrderResponse;
import com.myorders.makeorder.data_entities.response.Response;
import com.myorders.makeorder.data_entities.response.Result;

public class ResponseHelper {
    private static Gson gson = new Gson();

    public static String getErrorMesage(String json) {
        OrderResponse response = gson.fromJson(json, OrderResponse.class);
        return response.getResponse().getResult().getMessage();
    }

    public static OrderResponse getResponsePOJO(String responseString) {
        return gson.fromJson(responseString, OrderResponse.class);

    }

    public static String construct(String message) {
        OrderResponse response=new OrderResponse();
        response.setResponse(new Response());
        response.getResponse().setResult(new Result());
        response.getResponse().getResult().setMessage(message);
        return gson.toJson(response);
    }
}
