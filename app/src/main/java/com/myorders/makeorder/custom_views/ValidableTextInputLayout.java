package com.myorders.makeorder.custom_views;

import android.content.Context;
import android.content.res.TypedArray;
import android.databinding.BindingMethod;
import android.databinding.BindingMethods;
import android.databinding.InverseBindingListener;
import android.databinding.InverseBindingMethod;
import android.databinding.InverseBindingMethods;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.FrameLayout;

import com.github.reinaldoarrosi.maskededittext.MaskedEditText;
import com.myorders.makeorder.R;
import com.myorders.makeorder.validation.Validable;
import com.myorders.makeorder.validation.Validator;

@InverseBindingMethods(
        @InverseBindingMethod(
                type = ValidableTextInputLayout.class,
                attribute = "app:text",
                method = "getText"
        )
)
@BindingMethods({
        @BindingMethod(
                type = ValidableTextInputLayout.class,
                attribute = "textAttrChanged",
                method = "setTextChangeListener"
        )
})
public class ValidableTextInputLayout extends FrameLayout implements Validable {
    private Validator validator;
    private EditText editText;
    private TextInputLayout til;
    private InverseBindingListener inverseBindingListener;
    private boolean isNumber = false;

    public void setTextChangeListener(InverseBindingListener listener) {
        inverseBindingListener = listener;
    }

    public ValidableTextInputLayout(Context context) {
        super(context);
    }

    public ValidableTextInputLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.ValidableTextInputLayout,
                0, 0);

        try {
            isNumber = a.getBoolean(R.styleable.ValidableTextInputLayout_phone, false);
        } finally {
            a.recycle();
        }
        if (isNumber) {
            inflater.inflate(R.layout.number_edit_text, this, true);

        } else
            inflater.inflate(R.layout.validable_edit_text, this, true);
        til = (TextInputLayout) getChildAt(0);
        editText = (EditText) ((FrameLayout) til.getChildAt(0)).getChildAt(0);
        if (isNumber) {
            MaskedEditText met = (MaskedEditText) editText;
            met.setMask("+9(999) 999-99-99");
        }
        editText.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    editText.post(new Runnable() {
                        @Override
                        public void run() {
                            editText.setSelection(editText.getEditableText().toString().length());
                        }
                    });
                }
            }
        });
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (inverseBindingListener != null)
                    inverseBindingListener.onChange();
            }

            @Override
            public void afterTextChanged(Editable editable) {


                til.setError(null);
            }
        });
    }

    public void setLast(boolean b) {
        if (b)
            editText.setImeOptions(EditorInfo.IME_ACTION_DONE);
    }

    public void setHint(String hint) {
        til.setHint(hint);
    }


    public void setText(String text) {
        editText.setText(text);
    }

    public String getText() {
        return editText.getEditableText().toString();
    }

    public void setValidator(Validator validator) {
        this.validator = validator;
    }


    @Override
    public boolean validate() {
        if (validator != null) {
            String value = editText.getEditableText().toString();
            String res = validator.validate(value);
            if (res != null) {
                til.setError(res);
                return false;
            }
        }
        til.setError(null);
        return true;
    }

    private void formatNumber() {
        String s = editText.getEditableText().toString();
    }
}
