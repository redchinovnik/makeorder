package com.myorders.makeorder.custom_views;

import android.content.Context;
import android.databinding.BindingMethod;
import android.databinding.BindingMethods;
import android.databinding.InverseBindingListener;
import android.databinding.InverseBindingMethod;
import android.databinding.InverseBindingMethods;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.myorders.makeorder.R;
import com.myorders.makeorder.validation.Validable;
import com.myorders.makeorder.validation.Validator;


@InverseBindingMethods(
        @InverseBindingMethod(
                type = CustomSpinner.class,
                attribute = "app:value",
                method = "getValue"
        )
)
@BindingMethods({
        @BindingMethod(
                type = CustomSpinner.class,
                attribute = "valueAttrChanged",
                method = "setValueChangeListener"
        )
})
public class CustomSpinner extends FrameLayout {
    private TextView hint;
    private Spinner spinner;
    private InverseBindingListener inverseBindingListener;
    private String[] values;

    public void setValueChangeListener(InverseBindingListener listener) {
        inverseBindingListener = listener;
    }

    public CustomSpinner(Context context) {
        super(context);
    }

    public void setVisibleValues(String[] array) {
        ArrayAdapter<String> adapter =
                new ArrayAdapter<String>(this.getContext(), android.R.layout.simple_spinner_dropdown_item, array);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (inverseBindingListener != null)
                    inverseBindingListener.onChange();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                if (inverseBindingListener != null)
                    inverseBindingListener.onChange();
            }
        });
    }

    public void setValues(String[] array) {
        this.values = array;
    }

    public void setValue(String v) {
        if (values == null)
            return;
        for (int i = 0; i < values.length; i++) {
            if (v.equals(values[i])) {
                spinner.setSelection(i);
                return;
            }
        }
    }

    public String getValue() {
        if (values == null)
            return null;
        return values[spinner.getSelectedItemPosition()];
    }

    public CustomSpinner(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.custom_spiner, this, true);
        LinearLayout ll = (LinearLayout) getChildAt(0);
        hint = (TextView) ll.getChildAt(0);
        spinner = (Spinner) ll.getChildAt(1);
    }

    public void setHint(String hint) {
        this.hint.setText(hint);
    }
}
