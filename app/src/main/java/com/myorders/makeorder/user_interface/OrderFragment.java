package com.myorders.makeorder.user_interface;

import android.arch.lifecycle.Observer;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.myorders.makeorder.App;
import com.myorders.makeorder.NavigationController;
import com.myorders.makeorder.OrderViewModel;
import com.myorders.makeorder.R;
import com.myorders.makeorder.ResponseHelper;
import com.myorders.makeorder.data_entities.OrderRequest;
import com.myorders.makeorder.data_entities.StatusWrapper;
import com.myorders.makeorder.databinding.FragmentOrderBinding;
import com.myorders.makeorder.validation.Validable;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class OrderFragment extends Fragment {
    private List<Validable> validables = new ArrayList<>();
    @Inject
    public OrderViewModel orderViewModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FragmentOrderBinding binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_order, container, false);
        ((App) getActivity().getApplication()).getAppComponent().injectFragment(this);
        View view = binding.getRoot();
        findAllValidables(view);
        view.findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!validateAll())
                    validationFailed();
                else
                    makeOrder();
            }
        });

        OrderRequest orderRequest = orderViewModel.getOrderRequest(getContext());
        binding.setOrder(orderRequest);
        binding.setCtx(getActivity().getApplicationContext());

        orderViewModel.watchStatus().observe(this, new Observer<StatusWrapper>() {
            @Override
            public void onChanged(@Nullable StatusWrapper statusWrapper) {
                binding.setStatus(statusWrapper.getStatus());
                if (statusWrapper.getStatus() == StatusWrapper.Status.ERROR) {
                    dispatchError(statusWrapper);
                }
                if (statusWrapper.getStatus() == StatusWrapper.Status.SUCCESS) {
                    dispatchSuccess(statusWrapper);
                }
                if (statusWrapper.getStatus() != StatusWrapper.Status.PROCCESSING)
                    statusWrapper.setStatus(StatusWrapper.Status.IDDLE);
                Log.w("xxx", "x " + statusWrapper.getStatus() + "  " + statusWrapper.getResponseString());
            }
        });

        return view;
    }

    private void dispatchSuccess(StatusWrapper statusWrapper) {
        NavigationController.navigateToSuccess(getActivity());
    }

    private void dispatchError(StatusWrapper statusWrapper) {
        String errorMessage = ResponseHelper.getErrorMesage(statusWrapper.getResponseString());
        showAlert(errorMessage);
    }

    private void showAlert(String message) {
        ErrorDialog errorDialog = new ErrorDialog();
        Bundle bundle = new Bundle();
        bundle.putString(ErrorDialog.KEY_MESSAGE, message);
        errorDialog.setArguments(bundle);
        errorDialog.show(getActivity().getSupportFragmentManager(), "");
    }


    private void makeOrder() {
        orderViewModel.makeOrder();
    }

    private void validationFailed() {
        showAlert(getString(R.string.dlg_validation_error));
    }

    private boolean validateAll() {
        boolean validated = true;
        for (Validable validable : validables)
            validated = validable.validate() && validated;

        return validated;
    }

    private void findAllValidables(View view) {
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            int cc = viewGroup.getChildCount();
            for (int i = 0; i < cc; i++) {
                View v = viewGroup.getChildAt(i);
                if (v instanceof Validable)
                    validables.add((Validable) v);
                findAllValidables(v);
            }
        }
    }
}

