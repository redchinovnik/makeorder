package com.myorders.makeorder.user_interface;

import android.content.Context;
import android.content.Intent;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;

import com.myorders.makeorder.R;

public class SuccessActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_temp);
        getSupportFragmentManager().beginTransaction().replace(R.id.content, new SuccessFragment()).commit();
    }

    public static Intent getCallingIntent(Context context) {
        Intent intent = new Intent(context, SuccessActivity.class);
        return intent;
    }



}
