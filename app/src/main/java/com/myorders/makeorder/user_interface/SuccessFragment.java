package com.myorders.makeorder.user_interface;

import android.arch.lifecycle.Observer;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.myorders.makeorder.App;
import com.myorders.makeorder.NavigationController;
import com.myorders.makeorder.OrderViewModel;
import com.myorders.makeorder.R;
import com.myorders.makeorder.ResponseHelper;
import com.myorders.makeorder.data_entities.StatusWrapper;
import com.myorders.makeorder.data_entities.response.OrderResponse;
import com.myorders.makeorder.databinding.FragmentOrderBinding;
import com.myorders.makeorder.databinding.FragmentSuccessBinding;

import javax.inject.Inject;

public class SuccessFragment extends Fragment {
    @Inject
    public OrderViewModel orderViewModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FragmentSuccessBinding binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_success, container, false);
        ((App) getActivity().getApplication()).getAppComponent().injectSuccessFragment(this);
        binding.setOrder(orderViewModel.getOrderRequest(getContext()));
        orderViewModel.watchStatus().observe(this, new Observer<StatusWrapper>() {
            @Override
            public void onChanged(@Nullable StatusWrapper statusWrapper) {
                OrderResponse response= ResponseHelper.getResponsePOJO(statusWrapper.getResponseString());
                binding.setResponse(response);
            }
        });
        View view = binding.getRoot();
        binding.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
        return view;
    }
}
