package com.myorders.makeorder;

import android.arch.core.util.Function;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;
import android.content.Context;
import android.util.Log;

import com.myorders.makeorder.data_entities.OrderRequest;
import com.myorders.makeorder.data_entities.StatusWrapper;

public class OrderViewModel {
    private OrderMaker orderMaker;
    private OrderRequest orderRequest;
    private MutableLiveData<String> paramsLiveData = new MutableLiveData<>();
    private LiveData<StatusWrapper> statusWrapperMutableLiveData = Transformations
            .switchMap(paramsLiveData, s -> orderMaker.makeOrder(s));

    public OrderViewModel(OrderMaker orderMaker) {
        this.orderMaker = orderMaker;
    }

    public LiveData<StatusWrapper> watchStatus() {
        return statusWrapperMutableLiveData;
    }

    public void makeOrder() {

        Log.w("xxx",orderRequest.toJson());
        paramsLiveData.setValue(orderRequest.toJson());
    }

    public OrderRequest getOrderRequest(Context context) {
        if (orderRequest == null)
            orderRequest = new OrderRequest(context);
        return orderRequest;
    }
}
