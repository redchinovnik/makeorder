package com.myorders.makeorder;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface OrderRESTService {

    @Headers({
            "POST: http://api.dev2.tanuki.ru/ HTTP/1.1",
            "Content-Type: application/x-www-form-urlencoded",
            "User-Agent: Tanuki/Test_app"

    })
    @FormUrlEncoded
    @POST(".")
    Call<String> makeOrder(@Field("jsonData") String data);

    @GET(".")
    Call<String> makeOrder1( );

}
