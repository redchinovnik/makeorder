package com.myorders.makeorder.validation;

import android.content.Context;

import com.myorders.makeorder.R;

public class EmailValidator implements Validator {
    private Context context;

    public EmailValidator(Context context){
        this.context=context;
    }
    @Override
    public String validate(String s) {
        if (s.length() < 1)
            return context.getResources().getString(R.string.val_no_email);
        if(!org.apache.commons.validator.routines.EmailValidator.getInstance().isValid(s))
            return context.getResources().getString(R.string.val_email_error);
        return null;
    }

    public static EmailValidator build(Context context){
        return new EmailValidator(context);
    }
}
