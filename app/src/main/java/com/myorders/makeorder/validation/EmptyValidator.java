package com.myorders.makeorder.validation;

import android.content.Context;

import com.myorders.makeorder.R;

public class EmptyValidator implements Validator {
    private String errorMessage;

    public EmptyValidator(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    @Override
    public String validate(String s) {
        s=s.trim();
        if (s.length() < 1)
            return errorMessage;
        return null;
    }

    public static EmptyValidator build(String message) {
        return new EmptyValidator(message);
    }
}
