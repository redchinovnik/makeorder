package com.myorders.makeorder.validation;

public interface Validator {
    String validate(String s);
}
