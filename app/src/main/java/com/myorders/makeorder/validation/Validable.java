package com.myorders.makeorder.validation;

public interface Validable {
    boolean validate();
}
