package com.myorders.makeorder.validation;

import android.content.Context;
import android.util.Log;

import com.myorders.makeorder.R;

public class PhoneNumberValidator implements Validator {
    private Context context;

    public PhoneNumberValidator(Context context) {
        this.context = context;
    }

    @Override
    public String validate(String s) {
        s=s.replace(" ","");
        if (s.length() < 1)
            return context.getResources().getString(R.string.val_no_pn);
        if (s.length() !=16)
            return context.getResources().getString(R.string.val_pn_error);
        return null;
    }

    public static PhoneNumberValidator build(Context context) {
        return new PhoneNumberValidator(context);
    }
}
