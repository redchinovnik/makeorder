package com.myorders.makeorder;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.myorders.makeorder.user_interface.OrderFragment;

public class OrderActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temp);
        getSupportFragmentManager().beginTransaction().replace(R.id.content, new OrderFragment()).commit();
    }

    public static Intent getCallingIntent(Context context) {
        Intent intent = new Intent(context, OrderActivity.class);
        return intent;
    }
}
