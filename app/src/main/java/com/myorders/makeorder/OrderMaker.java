package com.myorders.makeorder;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;

import com.myorders.makeorder.data_entities.StatusWrapper;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderMaker {
    private OrderRESTService orderRESTService;

    public OrderMaker(OrderRESTService orderRESTService) {
        this.orderRESTService = orderRESTService;
    }

    public LiveData<StatusWrapper> makeOrder(String jsonData) {
        final StatusWrapper statusWrapper = new StatusWrapper();
        statusWrapper.setStatus(StatusWrapper.Status.PROCCESSING);
        final MutableLiveData<StatusWrapper> res = new MutableLiveData<>();
        res.setValue(statusWrapper);
        orderRESTService.makeOrder(jsonData).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                try {
                    if (!response.isSuccessful()) {
                        statusWrapper.setStatus(StatusWrapper.Status.ERROR);
                        statusWrapper.setResponseString(response.errorBody().string());
                    } else {
                        statusWrapper.setStatus(StatusWrapper.Status.SUCCESS);
                        statusWrapper.setResponseString(response.body());
                    }
                } catch (Exception ex) {
                    statusWrapper.setStatus(StatusWrapper.Status.ERROR);
                    String s=ResponseHelper.construct(ex.getMessage());
                    statusWrapper.setResponseString(s);
                }
                res.setValue(statusWrapper);
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                statusWrapper.setStatus(StatusWrapper.Status.ERROR);
                String s=ResponseHelper.construct(t.getMessage());
                statusWrapper.setResponseString(s);
                res.setValue(statusWrapper);
            }
        });
        return res;
    }
}
